var listElement = document.querySelector("#app ul");
var inputElement = document.querySelector("#app input");
var buttonElement = document.querySelector("#app button");


var todos = [
    'Fazer café',
    'Estudar curso Rocketseat',
    'Conquistar sucesso'
];

function renderTodos(){
    //apaga tudo que esta dentro da <ul> para nao ficar repetindo os mesmos todos já existentes sempre que um novo for add 
    listElement.innerHTML = "";


    for (todo of todos){
        var todoElement =  document.createElement('li');
        var todoText = document.createTextNode(todo);

        var linkElement = document.createElement('button');

        //retorna o indice do exato todo selecionado para pegar sua posição
        var pos = todos.indexOf(todo);
        linkElement.setAttribute('onclick', 'deleteTodo(' + pos + ')');

        var linkText = document.createTextNode('Deletar');


        linkElement.appendChild(linkText);

        todoElement.appendChild(todoText);
        todoElement.appendChild(linkElement);
        listElement.appendChild(todoElement);
    }
}

renderTodos();

function addTodo(){
    var todoText = inputElement.value;

    todos.push(todoText);
    inputElement.value ="";

    renderTodos();
}

buttonElement.onclick = addTodo;

function deleteTodo(pos){
    todos.splice(pos, 1);
    renderTodos();
}